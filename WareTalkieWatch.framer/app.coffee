# This imports all the layers for "x" into xLayers
xLayers = Framer.Importer.load "imported/x"

# Set background
bg = new BackgroundLayer 
	backgroundColor: "#fff"

# Create PageComponent
page = new PageComponent 
	width: 300
	height: 300
	scrollVertical: false
	borderRadius: 6
	
page.center()	

# Create layers in a for-loop
layer = new Layer 
	superLayer: page.content
	width: 250
	height: 250
	x: 30
	backgroundColor: "#bfefcc"
	borderRadius: 360
	opacity: 0.3
keys = Object.keys(xLayers)
xLayers[keys[0]].superLayer = layer
xLayers[keys[0]].width = 250
xLayers[keys[0]].height = 250
xLayers[keys[0]].opacity = 0.3
xLayers[keys[0]].x = 0
xLayers[keys[0]].y = 0
speakText = new Layer
	superLayer: page.content
	width: 200
	backgroundColor: "#fff"
	y: 260
	x: 90
speakText.html = "<p style='color:black'>S P E A K</p>"


layer2 = new Layer 
	superLayer: page.content
	width: 250
	height: 250
	backgroundColor: "#b487d1"
	borderRadius: 360
	opacity: 0.3
	x: 270
pingText = new Layer
	superLayer: page.content
	width: 150
	backgroundColor: "#fff"
	y: 260
	x: 350
pingText.html = "<p style='color:black'>P I N G</p>"
	
# This imports all the layers for "x" into xLayers
xLayers2 = Framer.Importer.load "imported/x"
keys = Object.keys(xLayers2)
xLayers2[keys[0]].superLayer = layer2
xLayers2[keys[0]].width = 250
xLayers2[keys[0]].height = 250
xLayers2[keys[0]].opacity = 0.3
xLayers2[keys[0]].x = 0
xLayers2[keys[0]].y = 0
	
# Staging
page.snapToNextPage()
page.currentPage.opacity = 1

# Update pages
page.on "change:currentPage", ->
	page.previousPage.animate 
		properties:
			opacity: 0.3
		time: 0.4
		
	page.currentPage.animate 
		properties:
			opacity: 1
		time: 0.4