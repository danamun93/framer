jacksonLayers10 = Framer.Importer.load "imported/jackson"
brianLayers9 = Framer.Importer.load "imported/brian"
mitchellLayers8 = Framer.Importer.load "imported/mitchell"
aprilLayers7 = Framer.Importer.load "imported/april"
danaLayers5 = Framer.Importer.load "imported/dana"

# Set background
bg = new BackgroundLayer backgroundColor: "#85d6ee"
bg.bringToFront()

# Variables
rows = 6
gutter = 10
width  = Screen.width
height = 300

# Loop to create layers
#for index in [0...rows]	
	#cell = new Layer
		#width:  width
		#height: height 
		#y: 150 + index * (height + gutter)
		#borderRadius: 6
		#backgroundColor: "#fff"
	#cell.centerX()

Title = new Layer
	width:  width
	height: 100 
	y: 0
	backgroundColor: "#"
Title.centerX()
Title.html = "<br><h1 style='color: gray; text-align: center; font-size:90px; padding:20px'>Home Depot</h1>"
	
cell0 = new Layer
	width:  width
	height: 80 
	y: 150 + 0 * (height + gutter)
	borderRadius: 6
	backgroundColor: "#fff"
cell0.centerX()
cell0.html = "<br><h1 style='color: gray; text-align: center; padding: 1px'>Lumber Department</h1>"

cell1 = new Layer
	width:  width
	height: height 
	y: 150 + 0.3 * (height + gutter)
	borderRadius: 6
	backgroundColor: "#fff"
cell1.centerX()

circleSize = cell1.height - 40

cell1Picture = new Layer
	superLayer: cell1
	x: 50
	y: 20
	width: circleSize
	height: circleSize
	borderRadius: 360
# This imports all the layers for "worker" into workerLayers
keys = Object.keys(danaLayers5)
danaLayers5[keys[0]].superLayer = cell1Picture
danaLayers5[keys[0]].width = cell1Picture.width
danaLayers5[keys[0]].height = cell1Picture.height

cell1Name = new Layer
	superLayer: cell1
	x: 50 + circleSize + 50 
	y: 50
	width: 700
	height: 300
	backgroundColor: "#fff"
cell1Name.html = "<br><h1 style='color:black'>Name: Dana</h1> <br><h2 style='color:gray'>Proximity: close</h2>"
cell1Button = new Layer
	superLayer: cell1
	x: 50 + circleSize + 500
	y: 50
	width: 250
	height: 110
	borderRadius: 30
	backgroundColor: "#bfefcc"
cell1Button.html = "<br><h1 style='color:#b487d1; text-align: center; padding:10px'>P I N G</h1>"

cellTools = new Layer
	width:  width
	height: 80
	y: 150 + 1.3 * (height + gutter)
	borderRadius: 6
	backgroundColor: "#fff"
cellTools.centerX()
cellTools.html = "<br><h1 style='color: gray; text-align: center; padding: 1px'>Tools Department</h1>"

cell2 = new Layer
	width:  width
	height: height 
	y: 150 + 1.6 * (height + gutter)
	borderRadius: 6
	backgroundColor: "#fff"
cell2.centerX()

cell2Picture = new Layer
	superLayer: cell2
	x: 50
	y: 20
	width: circleSize
	height: circleSize
	borderRadius: 360
# This imports all the layers for "worker" into workerLayers
keys = Object.keys(aprilLayers7)
aprilLayers7[keys[0]].superLayer = cell2Picture
aprilLayers7[keys[0]].width = cell2Picture.width
aprilLayers7[keys[0]].height = cell2Picture.height

cell2Name = new Layer
	superLayer: cell2
	x: 50 + circleSize + 50 
	y: 50
	width: 700
	height: 300
	backgroundColor: "#fff"
cell2Name.html = "<br><h1 style='color:black'>Name: April</h1> <br><h2 style='color:gray'>Proximity: far</h2>"
cell2Button = new Layer
	superLayer: cell2
	x: 50 + circleSize + 500
	y: 50
	width: 250
	height: 110
	borderRadius: 30
	backgroundColor: "#bfefcc"
cell2Button.html = "<br><h1 style='color:#b487d1; text-align: center; padding:10px'>P I N G</h1>"

cell3 = new Layer
	width:  width
	height: height 
	y: 150 + 2.6 * (height + gutter)
	borderRadius: 6
	backgroundColor: "#fff"
cell3.centerX()

cell3Picture = new Layer
	superLayer: cell3
	x: 50
	y: 20
	width: circleSize
	height: circleSize
	borderRadius: 360

keys = Object.keys(mitchellLayers8)
mitchellLayers8[keys[0]].superLayer = cell3Picture
mitchellLayers8[keys[0]].width = cell3Picture.width
mitchellLayers8[keys[0]].height = cell3Picture.height

cell3Name = new Layer
	superLayer: cell3
	x: 50 + circleSize + 50 
	y: 50
	width: 700
	height: 300
	backgroundColor: "#fff"
cell3Name.html = "<br><h1 style='color:black'>Name: Mitchell</h1> <br><h2 style='color:gray'>Proximity: close</h2>"
cell3Button = new Layer
	superLayer: cell3
	x: 50 + circleSize + 500
	y: 50
	width: 250
	height: 110
	borderRadius: 30
	backgroundColor: "#bfefcc"
cell3Button.html = "<br><h1 style='color:#b487d1; text-align: center; padding:10px'>P I N G</h1>"

cell4 = new Layer
	width:  width
	height: height 
	y: 150 + 3.6 * (height + gutter)
	borderRadius: 6
	backgroundColor: "#fff"
cell4.centerX()

cell4Picture = new Layer
	superLayer: cell4
	x: 50
	y: 20
	width: circleSize
	height: circleSize
	borderRadius: 360

keys = Object.keys(brianLayers9)
brianLayers9[keys[0]].superLayer = cell4Picture
brianLayers9[keys[0]].width = cell4Picture.width
brianLayers9[keys[0]].height = cell4Picture.height

cell4Name = new Layer
	superLayer: cell4
	x: 50 + circleSize + 50 
	y: 50
	width: 700
	height: 300
	backgroundColor: "#fff"
cell4Name.html = "<br><h1 style='color:black'>Name: Bryan</h1> <br><h2 style='color:gray'>Proximity: close</h2>"
cell4Button = new Layer
	superLayer: cell4
	x: 50 + circleSize + 500
	y: 50
	width: 250
	height: 110
	borderRadius: 30
	backgroundColor: "#bfefcc"
cell4Button.html = "<br><h1 style='color:#b487d1; text-align: center; padding:10px'>P I N G</h1>"

cellAppliances = new Layer
	width:  width
	height: 80
	y: 150 + 4.6 * (height + gutter)
	borderRadius: 6
	backgroundColor: "#fff"
cellAppliances.centerX()
cellAppliances.html = "<br><h1 style='color: gray; text-align: center; padding: 1px'>Appliances Department</h1>"

cell5 = new Layer
	width:  width
	height: height 
	y: 150 + 4.9 * (height + gutter)
	borderRadius: 6
	backgroundColor: "#fff"
cell5.centerX()

cell5Picture = new Layer
	superLayer: cell5
	x: 50
	y: 20
	width: circleSize
	height: circleSize
	borderRadius: 360

keys = Object.keys(jacksonLayers10)
jacksonLayers10[keys[0]].superLayer = cell5Picture
jacksonLayers10[keys[0]].width = cell5Picture.width
jacksonLayers10[keys[0]].height = cell5Picture.height

cell5Name = new Layer
	superLayer: cell5
	x: 50 + circleSize + 50 
	y: 50
	width: 700
	height: 300
	backgroundColor: "#fff"
cell5Name.html = "<br><h1 style='color:black'>Name: Jackson</h1> <br><h2 style='color:gray'>Proximity: far</h2>"
cell5Button = new Layer
	superLayer: cell5
	x: 50 + circleSize + 500
	y: 50
	width: 250
	height: 110
	borderRadius: 30
	backgroundColor: "#bfefcc"
cell5Button.html = "<br><h1 style='color:#b487d1; text-align: center; padding:10px'>P I N G</h1>"