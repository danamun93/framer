bryan2Layers = Framer.Importer.load "imported/bryan2"
april2Layers = Framer.Importer.load "imported/april2"
bryanLayers = Framer.Importer.load "imported/bryan"
aprilLayers = Framer.Importer.load "imported/april"
workersLayers = Framer.Importer.load "imported/workers"
jacksonLayers = Framer.Importer.load "imported/jackson"
danaLayers = Framer.Importer.load "imported/dana"


#PAGE COMPONENT
page =  new PageComponent 
	width: 320
	height: 320
	backgroundColor: "#FFF"
	scrollVertical: false
	borderRadius: 320/2
page.center()
page.borderRadius = page.width/2
page.x += 2.5

# ADD SKIN ON TOP OF EVERYTHING ELSE
moto360 = new Layer
	width: 600, height: 900
	image: "images/moto_moto360-mask.png"
moto360.scaleX = 0.60
moto360.scaleY = 0.60
moto360.center()

# global variables
standardX = 320
standardY = 320
white = "#fff"
gray = "#bbb"
purple = "#b487d1"
green = "#bfefcc"
darkGreen = "#83ca92"
blue = "#85d6ee"

checkInLayer = new Layer
	superLayer: page.content
	width: standardX
	height: standardY
	backgroundColor: white
	borderRadius: standardX/2
	opacity: 1
	x: 0

checkInButton = new Layer
	superLayer: checkInLayer
	borderRadius: standardX/2
	backgroundColor: blue
	width: standardX - 70
	height: standardY - 70
	opacity: 0.7

checkInButton.centerX()
checkInButton.centerY()
checkInButton.html = "<p style='color:black; text-align: center; font-size: 35px'><br><br><br><br>Check In.</p>"

checkInButton.on Events.Click, ->
		checkInButton.html = "<p style='color:black; text-align: center; font-size: 35px'><br><br><br><br>Check Out.</p>"

	

broadcastLayer = new Layer 
	superLayer: page.content
	width: standardX
	height: standardY
	backgroundColor: white
	borderRadius: standardX/2
	opacity: 1
	y: standardY

# Variables for broadcast scroll
rows = 4
cols = 2
gutter = 10
width  = 100
height = 100

# Wrapper that will center the grid
wrapper = new ScrollComponent
	superLayer: broadcastLayer
	backgroundColor: "transparent" 
	clip: true
	width: standardX
	height: standardY
	y: 200
	x: 50

titleLayer = new Layer
	superLayer: wrapper.content
	backgroundColor: "red"
	opacity: 0.7
	width: standardX
	height: 60
titleLayer.html = "<p style='color:white; text-align: center; font-size: 22px'><br>B r o a d c a s t</p>"

departmentList = [["<p style='color:gray; text-align: center; font-size: 16px'><br>Appliances</p>", "<p style='color:gray; text-align: center; font-size: 16px'><br>Bath</p>"],
["<p style='color:gray; text-align: center; font-size: 16px'><br>Electrical</p>","<p style='color:gray; text-align: center; font-size: 15px'><br>Flooring</p>"],
["<p style='color:gray; text-align: center; font-size: 15px'><br>Kitchen</p>","<p style='color:gray; text-align: center; font-size: 15px'><br>Lawn</p>"],
["<p style='color:gray; text-align: center; font-size: 13px'><br>Outdoor Living</p>","<p style='color:gray; text-align: center; font-size: 15px'><br>Paint</p>"],
["<p style='color:gray; text-align: center; font-size: 15px'><br>Storage</p>","<p style='color:gray; text-align: center; font-size: 15px'><br>Tools</p>"]]

emergencyCodesList = [["<p style='color:gray; text-align: center; font-size: 16px'><br>Code Adam</p>","<p style='color:gray; text-align: center; font-size: 16px'><br>Code Black</p>"],["<p style='color:gray; text-align: center; font-size: 16px'><br>Code Blue</p>","<p style='color:gray; text-align: center; font-size: 16px'><br>Code Brown</p>"],["<p style='color:gray; text-align: center; font-size: 16px'><br>Code Green</p>","<p style='color:gray; text-align: center; font-size: 15px'><br>Code Orange</p>"],["<p style='color:gray; text-align: center; font-size: 16px'><br>Red</p>","<p style='color:gray; text-align: center; font-size: 16px'><br>300</p>"]]

# Create the grid layers
for rowIndex in [0...rows]
	for colIndex in [0...cols]
		cell = new Layer
			width:  width
			height: height
			x: colIndex * (width + gutter) + 50
			y: rowIndex * (height + gutter) + 70
			borderRadius: standardX/2
			opacity: 0.7
			backgroundColor: white
			borderColor: darkGreen
			borderWidth: 2
			superLayer: wrapper.content
		cell.html = emergencyCodesList[rowIndex][colIndex]

padding = new Layer
	superLayer: wrapper.content
	backgroundColor: white
	width: standardX
	height: 50
	y: 5 * (height + gutter) + 70
			
# Center the wrapper
wrapper.center()

broadcastSendLayer = new Layer
	superLayer: page.content
	backgroundColor: white
	x: broadcastLayer.x
	y: broadcastLayer.y + standardY
	width: standardX
	height: standardY
sendCircle = new Layer
	superLayer: broadcastSendLayer
	borderRadius: standardX/2
	width: standardX - 70
	height: standardY - 70
	backgroundColor: blue
	opacity: 0.7
sendCircle.center()	
sendCircle.html = "<p style='color:black; text-align: center; font-size: 22px; padding:25px'><br>Broadcast</p><p style='color:black;text-align: center; font-size: 30px'>Code: Adam"

decoLayer = new Layer
	superLayer: sendCircle
	backgroundColor: white
	opacity: 0.5
	width: 250
	height: 50
	y: -100

cancel = new Layer
	superLayer: broadcastSendLayer
	x: 90
	y: 200
	width: 65
	height: 65
	borderRadius: 65/2
	backgroundColor: "#bbb"
cancel.html = "<p style='color:white; text-align: center; font-size: 17px; padding-top: 15px'>Cancel</p>"

ping = new Layer
 	superLayer: broadcastSendLayer
 	x: 90 + 80
 	y: 200
 	width: 65
 	height: 65
 	borderRadius: 65/2
 	opacity: 0.7
 	backgroundColor: "red"
ping.html = "<p style='color:white; text-align: center; font-size: 17px; padding-top:15px'>Ping</p>"

broadcastNotificationLayer = new Layer
	superLayer: page.content
	x: broadcastSendLayer.x
	y: broadcastSendLayer.y + standardY
	backgroundColor: white
	width: standardX
	height: standardY
	
redDecoLayer = new Layer
	superLayer: broadcastNotificationLayer
	width: standardX
	backgroundColor: "red"
	height: 140
	
notificationText = new Layer
	superLayer: broadcastNotificationLayer
	width: standardX
	height: standardX - 115
	backgroundColor: "transparent"
	y: 140
	
notificationText.html = "<p style='color:red; text-align: left; font-size: 15px'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;New Message</p><p style='color:grey;text-align: left; font-size: 24px; padding-top: 5px; padding-left: 40px'>Code: Adam</p>"

departmentLayer = new Layer
	superLayer: page.content
	x: broadcastLayer.x + standardX
	y: broadcastLayer.y
	width: standardX
	height: standardY
	backgroundColor: white
	
# Variables for broadcast scroll
rows = 5
cols = 2
gutter = 10
width  = 100
height = 100

# Wrapper that will center the grid
wrapper = new ScrollComponent
	superLayer: departmentLayer
	backgroundColor: "transparent" 
	clip: true
	width: standardX
	height: standardY
	y: 200
	x: 50

titleLayer = new Layer
	superLayer: wrapper.content
	backgroundColor: purple
	opacity: 0.7
	width: standardX
	height: 60
titleLayer.html = "<p style='color:white; text-align: center; font-size: 21px'><br>D e p a r t m e n t</p>"

# Create the grid layers
for rowIndex in [0...rows]
	for colIndex in [0...cols]
		cell = new Layer
			width:  width
			height: height
			x: colIndex * (width + gutter) + 50
			y: rowIndex * (height + gutter) + 70
			borderRadius: standardX/2
			opacity: 0.7
			backgroundColor: white
			borderColor: darkGreen
			borderWidth: 2
			superLayer: wrapper.content
		cell.html = departmentList[rowIndex][colIndex]

padding = new Layer
	superLayer: wrapper.content
	backgroundColor: white
	width: standardX
	height: 50
	y: 5 * (height + gutter) + 70
			
# Center the wrapper
wrapper.center()

departmentBroadcastSendLayer = new Layer
	superLayer: page.content
	backgroundColor: white
	x: departmentLayer.x
	y: departmentLayer.y + standardY
	width: standardX
	height: standardY
sendCircle = new Layer
	superLayer: departmentBroadcastSendLayer
	borderRadius: standardX/2
	width: standardX - 70
	height: standardY - 70
	backgroundColor: blue
	opacity: 0.7
sendCircle.center()	
sendCircle.html = "<p style='color:black; text-align: center; font-size: 20px;'><br><br>Department Broadcast</p><p style='color:black;text-align: center; font-size: 30px'><br>Tools</p>"

decoLayer = new Layer
	superLayer: sendCircle
	backgroundColor: white
	opacity: 0.5
	width: 250
	height: 50
	y: -100

cancel = new Layer
	superLayer: departmentBroadcastSendLayer
	x: 90
	y: 200
	width: 65
	height: 65
	borderRadius: 65/2
	backgroundColor: "#bbb"
cancel.html = "<p style='color:white; text-align: center; font-size: 17px; padding-top: 15px'>Cancel</p>"

ping = new Layer
 	superLayer: departmentBroadcastSendLayer
 	x: 90 + 80
 	y: 200
 	width: 65
 	height: 65
 	borderRadius: 65/2
 	backgroundColor: purple
ping.html = "<p style='color:white; text-align: center; font-size: 17px; padding-top:15px'>Ping</p>"

departmentNotificationLayer = new Layer
	superLayer: page.content
	x: departmentBroadcastSendLayer.x
	y: departmentBroadcastSendLayer.y + standardY
	backgroundColor: white
	width: standardX
	height: standardY
	
redDecoLayer = new Layer
	superLayer: departmentNotificationLayer
	width: standardX
	backgroundColor: "red"
	height: 140
	
keys = Object.keys(danaLayers)
danaLayers[keys[0]].superLayer = redDecoLayer
danaLayers[keys[0]].width = redDecoLayer.width
danaLayers[keys[0]].height = 320
danaLayers[keys[0]].y = -70
	
notificationText = new Layer
	superLayer: departmentNotificationLayer
	width: standardX
	height: standardX - 115
	backgroundColor: "transparent"
	y: 140
	
notificationText.html = "<p style='color:purple; text-align: left; font-size: 15px'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;New Message</p><p style='color:grey;text-align: left; font-size: 24px; padding-top: 5px'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Dana needs help at &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Tools Department</p>"

decline = new Layer
	superLayer: departmentNotificationLayer
	x: 90
	y: 230
	width: 65
	height: 65
	borderRadius: 65/2
	backgroundColor: "#bbb"
decline.html = "<p style='color:white; text-align: center; font-size: 17px; padding-top: 15px'>Decline</p>"

accept = new Layer
	superLayer: departmentNotificationLayer
	x: 70 + 100
	y: 230
	width: 65
	height: 65
	borderRadius: 65/2
	backgroundColor: purple
accept.html = "<p style='color:white; text-align: center; font-size: 17px; padding-top: 15px'>Accept</p>"

departmentNotificationReplyLayer = new Layer
	superLayer: page.content
	x: departmentNotificationLayer.x
	y: departmentNotificationLayer.y + standardY
	width: standardX
	height: standardY
	backgroundColor: white
redDecoLayer = new Layer
	superLayer: departmentNotificationReplyLayer
	width: standardX
	backgroundColor: white
	height: 140
	
keys = Object.keys(jacksonLayers)
jacksonLayers[keys[0]].superLayer = redDecoLayer
jacksonLayers[keys[0]].width = redDecoLayer.width
jacksonLayers[keys[0]].height = 320
jacksonLayers[keys[0]].y = -80
	
notificationText = new Layer
	superLayer: departmentNotificationReplyLayer
	width: standardX
	height: standardX - 115
	backgroundColor: white
	y: 140
	
notificationText.html = "<p style='color:purple; text-align: left; font-size: 15px'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;New Message</p><p style='color:grey;text-align: left; font-size: 24px; padding-top: 5px; padding-left: 40px'>\Jackson is on his way.</p>"

individualLayer = new Layer
	superLayer: page.content
	x: departmentLayer.x + standardX
	y: departmentLayer.y
	width: standardX
	height: standardY
	backgroundColor: white

# Variables for broadcast scroll
rows = 5
gutter = 10
width  = 320
height = 100

# Wrapper that will center the grid
wrapper = new ScrollComponent
	superLayer: individualLayer
	backgroundColor: "transparent" 
	clip: true
	width: standardX
	height: standardY

titleLayer = new Layer
	superLayer: wrapper.content
	backgroundColor: green
	width: standardX
	height: 60
titleLayer.html = "<p style='color:white; text-align: center; font-size: 21px'><br>I n d i v i d u a l s</p>"

individualList = ["<p style='padding-top: 15px;color:black; font-size: 16px; text-align: left;padding-left: 150px'>Name: Jackson</p><p style='color:gray; font-size: 13px; text-align:left; padding-left: 150px'>Proximity: close</p>","<p style='padding-top: 15px;color:black; font-size: 16px; text-align: left;padding-left: 150px'>Name: April</p><p style='color:gray; font-size: 13px; text-align:left; padding-left: 150px'>Proximity: far</p>","<p style='padding-top: 15px;color:black; font-size: 16px; text-align: left;padding-left: 150px'>Name: Bryan</p><p style='color:gray; font-size: 13px; text-align:left; padding-left: 150px'>Proximity: close</p>","<p style='padding-top: 15px;color:black; font-size: 16px; text-align: left;padding-left: 150px'>Name: Dana</p><p style='color:gray; font-size: 13px; text-align:left; padding-left: 150px'>Proximity: close</p>","<p style='padding-top: 15px;color:black; font-size: 16px; text-align: left;padding-left: 150px'>Name: Mitchell</p><p style='color:gray; font-size: 13px; text-align:left; padding-left: 150px'>Proximity: far</p>"]
keys = Object.keys(workersLayers)

# Create the grid layers
for rowIndex in [0...rows]
	cell = new Layer
		width:  width
		height: height
		x: -30
		y: rowIndex * (height + gutter) + 70
		borderRadius: standardX/2
		backgroundColor: white
		borderColor: darkGreen
		borderWidth: 2
		superLayer: wrapper.content
	
	workersLayers[keys[rowIndex]].superLayer = cell
	workersLayers[keys[rowIndex]].width = 80
	workersLayers[keys[rowIndex]].height = 80
	workersLayers[keys[rowIndex]].x = 60
	workersLayers[keys[rowIndex]].y = 5
	cell.html = individualList[rowIndex]
	

padding = new Layer
	superLayer: wrapper.content
	backgroundColor: white 
	width: standardX
	height: 50
	y: 5 * (height + gutter) + 100
			
# Center the wrapper
wrapper.center()

individualMessageLayer = new Layer
	superLayer: page.content
	x: individualLayer.x 
	y: individualLayer.y + standardY
	width: standardX
	height: standardY
	backgroundColor: white
	
# Variables for broadcast scroll
rows = 2
cols = 2
gutter = 10
width  = 100
height = 100

# Wrapper that will center the grid
wrapper = new ScrollComponent
	superLayer: individualMessageLayer
	backgroundColor: "transparent" 
	clip: true
	width: standardX
	height: standardY
	y: 200
	x: 50

titleLayer = new Layer
	superLayer: wrapper.content
	backgroundColor: green
	width: standardX
	height: 60
titleLayer.html = "<p style='color:white; text-align: center; font-size: 21px'><br>Create Message</p>"

messageList = [["<p style='color:gray; text-align: center; font-size: 16px'><br>Call Me</p>","<p style='color:gray; text-align: center; font-size: 16px'><br>Needed at Register</p>"],["<p style='color:gray; text-align: center; font-size: 16px'><br>Needed at CS</p>","<p style='color:gray; text-align: center; font-size: 16px'><br>Need you at Tools</p>"]]

# Create the grid layers
for rowIndex in [0...rows]
	for colIndex in [0...cols]
		cell = new Layer
			width:  width
			height: height
			x: colIndex * (width + gutter) + 50
			y: rowIndex * (height + gutter) + 70
			borderRadius: standardX/2
			opacity: 0.7
			backgroundColor: white
			borderColor: darkGreen
			borderWidth: 2
			superLayer: wrapper.content
		cell.html = messageList[rowIndex][colIndex]

padding = new Layer
	superLayer: wrapper.content
	backgroundColor: white
	width: standardX
	height: 50
	y: 5 * (height + gutter) + 70
			
# Center the wrapper
wrapper.center()

individualMessageSendLayer = new Layer
	superLayer: page.content
	backgroundColor: white
	x: individualMessageLayer.x
	y: individualMessageLayer.y + standardY
	width: standardX
	height: standardY
sendCircle = new Layer
	superLayer: individualMessageSendLayer
	borderRadius: standardX/2
	width: standardX - 70
	height: standardY - 70
	backgroundColor: blue
	opacity: 0.7
sendCircle.center()	
sendCircle.html = "<p style='color:black; text-align: center; font-size: 22px; padding:25px'><br>\"Call Me\"</p>"

individualInfoLayer = new Layer
	superLayer: individualMessageSendLayer
	y: 110
	width: standardX
	backgroundColor: "transparent"

keys = Object.keys(aprilLayers)
aprilLayers[keys[0]].superLayer = individualInfoLayer
aprilLayers[keys[0]].width = 70
aprilLayers[keys[0]].height = 70
aprilLayers[keys[0]].x = 60
aprilLayers[keys[0]].y = 5
individualInfoLayer.html = "<p style='padding-top: 20px;color:black; font-size: 16px; text-align: left;padding-left: 150px'>Name: April</p><p style='color:gray; font-size: 13px; text-align:left; padding-left: 150px'>Proximity: far</p>"


decoLayer = new Layer
	superLayer: sendCircle
	backgroundColor: white
	opacity: 0.5
	width: 250
	height: 50
	y: -65

cancel = new Layer
	superLayer: individualMessageSendLayer
	x: 90
	y: 200
	width: 65
	height: 65
	borderRadius: 65/2
	backgroundColor: "#bbb"
cancel.html = "<p style='color:white; text-align: center; font-size: 17px; padding-top: 15px'>Cancel</p>"

ping = new Layer
	superLayer: individualMessageSendLayer
	x: 90 + 80
	y: 200
	width: 65
	height: 65
	borderRadius: 65/2
	backgroundColor: darkGreen
ping.html = "<p style='color:white; text-align: center; font-size: 17px; padding-top:15px'>Ping</p>"


individualNotificationLayer = new Layer
	superLayer: page.content
	x: individualMessageSendLayer.x
	y: individualMessageSendLayer.y + standardY
	backgroundColor: white
	width: standardX
	height: standardY
	
redDecoLayer = new Layer
	superLayer: individualNotificationLayer
	width: standardX
	backgroundColor: darkGreen
	height: 140
	
keys = Object.keys(bryanLayers)
bryanLayers[keys[0]].superLayer = redDecoLayer
bryanLayers[keys[0]].width = redDecoLayer.width
bryanLayers[keys[0]].height = 320
bryanLayers[keys[0]].y = -70
	
notificationText = new Layer
	superLayer: individualNotificationLayer
	width: standardX
	height: standardX - 115
	backgroundColor: "transparent"
	y: 140
	
notificationText.html = "<p style='color:darkGreen; text-align: left; font-size: 15px'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;New Message from Bryan</p><p style='color:grey;text-align: left; font-size: 24px; padding-top: 5px; padding-left: 40px'>\"Call Me\"</p>"

reply = new Layer
	superLayer: individualNotificationLayer
	y: 210
	width: 80
	height: 80
	borderRadius: 80/2
	backgroundColor: darkGreen
reply.centerX()
reply.html = "<p style='color:white; text-align: center; font-size: 17px; padding-top: 20px'>Reply</p>"

## individual notification reply layer
individualNotificationReplyLayer = new Layer
	superLayer: page.content
	x: individualNotificationLayer.x 
	y: individualNotificationLayer.y + standardY
	width: standardX
	height: standardY
	backgroundColor: white
	
# Variables for broadcast scroll
rows = 2
cols = 2
gutter = 10
width  = 100
height = 100

# Wrapper that will center the grid
wrapper = new ScrollComponent
	superLayer: individualNotificationReplyLayer
	backgroundColor: "transparent" 
	clip: true
	width: standardX
	height: standardY
	y: 200
	x: 50

titleLayer = new Layer
	superLayer: wrapper.content
	backgroundColor: green
	width: standardX
	height: 60
titleLayer.html = "<p style='color:white; text-align: center; font-size: 21px'><br>Create Message</p>"

messageList = [["<p style='color:gray; text-align: center; font-size: 16px'><br>Not Now</p>","<p style='color:gray; text-align: center; font-size: 16px'><br>5 more min.</p>"],["<p style='color:gray; text-align: center; font-size: 16px'><br>10 more mins.</p>","<p style='color:gray; text-align: center; font-size: 16px'><br>Okay!</p>"]]

# Create the grid layers
for rowIndex in [0...rows]
	for colIndex in [0...cols]
		cell = new Layer
			width:  width
			height: height
			x: colIndex * (width + gutter) + 50
			y: rowIndex * (height + gutter) + 70
			borderRadius: standardX/2
			opacity: 0.7
			backgroundColor: white
			borderColor: darkGreen
			borderWidth: 2
			superLayer: wrapper.content
		cell.html = messageList[rowIndex][colIndex]

padding = new Layer
	superLayer: wrapper.content
	backgroundColor: white
	width: standardX
	height: 50
	y: 5 * (height + gutter) + 50
			
# Center the wrapper
wrapper.center()

individualNotificationReplySendLayer = new Layer
	superLayer: page.content
	backgroundColor: white
	x: individualNotificationReplyLayer.x
	y: individualNotificationReplyLayer.y + standardY
	width: standardX
	height: standardY
sendCircle = new Layer
	superLayer: individualNotificationReplySendLayer
	borderRadius: standardX/2
	width: standardX - 70
	height: standardY - 70
	backgroundColor: blue
	opacity: 0.7
sendCircle.center()	
sendCircle.html = "<p style='color:black; text-align: center; font-size: 22px; padding:25px'><br>\"5 more mins.\"</p>"

individualInfoLayer = new Layer
	superLayer: individualNotificationReplySendLayer
	y: 110
	width: standardX
	backgroundColor: "transparent"

keys = Object.keys(bryan2Layers)
bryan2Layers[keys[0]].superLayer = individualInfoLayer
bryan2Layers[keys[0]].width = 70
bryan2Layers[keys[0]].height = 70
bryan2Layers[keys[0]].x = 60
bryan2Layers[keys[0]].y = 5
individualInfoLayer.html = "<p style='padding-top: 20px;color:black; font-size: 16px; text-align: left;padding-left: 150px'>Name: Bryan</p><p style='color:gray; font-size: 13px; text-align:left; padding-left: 150px'>Proximity: far</p>"


decoLayer = new Layer
	superLayer: sendCircle
	backgroundColor: white
	opacity: 0.5
	width: 250
	height: 50
	y: -65

cancel = new Layer
	superLayer: individualNotificationReplySendLayer
	x: 90
	y: 200
	width: 65
	height: 65
	borderRadius: 65/2
	backgroundColor: "#bbb"
cancel.html = "<p style='color:white; text-align: center; font-size: 17px; padding-top: 15px'>Cancel</p>"

ping = new Layer
	superLayer: individualNotificationReplySendLayer
	x: 90 + 80
	y: 200
	width: 65
	height: 65
	borderRadius: 65/2
	backgroundColor: darkGreen
ping.html = "<p style='color:white; text-align: center; font-size: 17px; padding-top:15px'>Ping</p>"


individualNewNotificationLayer = new Layer
	superLayer: page.content
	x: individualNotificationReplySendLayer.x
	y: individualNotificationReplySendLayer.y + standardY
	backgroundColor: white
	width: standardX
	height: standardY
	
redDecoLayer = new Layer
	superLayer: individualNewNotificationLayer
	width: standardX
	backgroundColor: darkGreen
	height: 140
	
keys = Object.keys(april2Layers)
april2Layers[keys[0]].superLayer = redDecoLayer
april2Layers[keys[0]].width = redDecoLayer.width
april2Layers[keys[0]].height = 320
april2Layers[keys[0]].y = -115
	
notificationText = new Layer
	superLayer: individualNewNotificationLayer
	width: standardX
	height: standardX - 115
	backgroundColor: "transparent"
	y: 140
	
notificationText.html = "<p style='color:darkGreen; text-align: left; font-size: 15px'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;New Message from April</p><p style='color:grey;text-align: left; font-size: 24px; padding-top: 5px; padding-left: 40px'>\"5 more mins.\"</p>"