window.__imported__ = window.__imported__ || {};
window.__imported__["workers/layers.json.js"] = [
	{
		"id": 30,
		"name": "Group_5",
		"layerFrame": {
			"x": 0,
			"y": 0,
			"width": 186,
			"height": 187
		},
		"maskFrame": null,
		"image": {
			"path": "images/Group_5.png",
			"frame": {
				"x": 0,
				"y": 0,
				"width": 186,
				"height": 187
			}
		},
		"imageType": "png",
		"children": [
			
		],
		"modification": "74720530"
	},
	{
		"id": 28,
		"name": "Group_4",
		"layerFrame": {
			"x": 0,
			"y": 0,
			"width": 186,
			"height": 187
		},
		"maskFrame": null,
		"image": {
			"path": "images/Group_4.png",
			"frame": {
				"x": 0,
				"y": 0,
				"width": 186,
				"height": 187
			}
		},
		"imageType": "png",
		"children": [
			
		],
		"modification": "1043636407"
	},
	{
		"id": 26,
		"name": "Group_3",
		"layerFrame": {
			"x": 0,
			"y": 0,
			"width": 186,
			"height": 187
		},
		"maskFrame": null,
		"image": {
			"path": "images/Group_3.png",
			"frame": {
				"x": 0,
				"y": 0,
				"width": 186,
				"height": 187
			}
		},
		"imageType": "png",
		"children": [
			
		],
		"modification": "1210807121"
	},
	{
		"id": 24,
		"name": "Group_2",
		"layerFrame": {
			"x": 0,
			"y": 0,
			"width": 186,
			"height": 187
		},
		"maskFrame": null,
		"image": {
			"path": "images/Group_2.png",
			"frame": {
				"x": 0,
				"y": 1,
				"width": 186,
				"height": 186
			}
		},
		"imageType": "png",
		"children": [
			
		],
		"modification": "2037758706"
	},
	{
		"id": 36,
		"name": "Group_6",
		"layerFrame": {
			"x": 0,
			"y": 0,
			"width": 186,
			"height": 187
		},
		"maskFrame": null,
		"image": {
			"path": "images/Group_6.png",
			"frame": {
				"x": 0,
				"y": 1,
				"width": 186,
				"height": 186
			}
		},
		"imageType": "png",
		"children": [
			
		],
		"modification": "750114117"
	}
]